/*
----------------------------------------------------------------------------------------------------
	WaspInc: WASP Includes
	File: WaspInc.h

	This file is part of WaspSrvr. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WaspSrvr is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WaspSrvr is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WaspSrvr.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------------------
*/



/*
	WaspInc is WASP includes header file for WaspSrvr for the Internet of Things. To use it, modify
	the appropriate definitions in this header file. This header should already include all te requisite
	header files (including those which are which are part of this library). For any information, please
	visit: https://github.com/wolfram77/WaspSrvr.
*/



#ifndef	_WaspInc_h_
#define	_WaspInc_h_



// Platform:
// 
// Select the platform under use, here it is PC
// 
// For PC:		Set lib_Platform to lib_PlatformPC
// For AVR:		Set lib_Platform to lib_PlatformAVR
//
#define	lib_PlatformPC			1
#define	lib_PlatformAVR			2
#define	lib_Platform			lib_PlatformPC
#define	lib_WordSize			64



// AVR headers
#if lib_Platform == lib_PlatformAVR
#include <avr/io.h>
#include <avr/interrupt.h>
#include <Arduino.h>
#endif

// PC Server headers
#if lib_Platform == lib_PlatformPC
// standard headers
#include <stdio.h>
#include <string.h>
// winsock headers
#include <WinSock2.h>
#include <WS2tcpip.h>

// winsock library
#pragma comment(lib, "Ws2_32.lib")
#endif



// Library shorthand options
// 
// 0 -	No shorthanding
//		All functions and variables can only be accessed as <lib_name>_<function_name>
//		e.g- "Memory" module's "ReadInt" function can be accessed as "memory_ReadInt"
// 
// 1 -	Level 1 shorthanding
//		All functions and variables can also be accessed as <lib_shortname><function_name>
//		e.g- "Memory" module's "ReadInt" function can be accessed as "memReadInt"
// 
// 2 -	Level 2 shorthanding
//		All functions and variables can also be accessed as <function_name>
//		e.g- "Memory" module's "ReadInt" function can be accessed as "ReadInt"
#define	macro_Shorthand			2
#define	type_Shorthand			2
#define	memory_Shorthand		2
#define	register_Shorthand		1
#define	expint_Shorthand		1
#define	list_Shorthand			1
#define	task_Shorthand			1
#define	stream_Shorthand		1



// Memory Module options
// 
//	Select how many bytes Type uses for
//  type conversion.
#define	type_Mold				type_Mold16



// Task Module options
// 
//	Select how many bytes of save space is available for each
//  task in default mold (task_Mold)
#define	task_Mold				task_Mold2
//	Select how many max. tasks can be run concurrently
#define	list_TaskListMold		list_TaskListMold8



// Stream Module options
// 
//	Select how many bytes of space is available in each stream
// in default mold (stream_Mold)
#define	stream_Mold				stream_Mold32



// Include Std headers
#include "Macro.h"
#include "Type.h"
#include "Register.h"
#include "Memory.h"
#include "ExpInt.h"



// Include WASP headers
#include "WaspProp.h"



#endif // !_WaspInc_h_
