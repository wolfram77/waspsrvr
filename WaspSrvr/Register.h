/*
----------------------------------------------------------------------------------------
	Register: Register operations module
	File: Register.h

	This file is part of WaspSrvr. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WaspSrvr is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WaspSrvr is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WaspSrvr.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/



/*
	Register is a register operations module for WaspSrvr. It has been developed for
	for being used in the development of the internet of things. For information,
	on its usage, please visit: https://github.com/wolfram77/WaspSrvr.
*/



#ifndef	_Register_h_
#define	_Register_h_



// Requisite headers
#include "Macro.h"



// Select shorthand level
// 
// The default shorthand level is 1 i.e., members of this
// module can be accessed as reg<function_name> directly.
// The shorthand level can be selected in the main header
// file of WaspSrvr library
#ifndef	register_Shorthand
#define	register_Shorthand	1
#endif



// Function:
// ReadBit(src, bit_no)
// 
// Returns the value of bit at the specified bit number (bit_no)
// from the specified source register (src)
// 
// Parameters:
// src:		the source register
// bit_no:	the index of the bit (starts from 0)
// 
// Returns:
// bit_value:	the value of the specified bit (0 or 1)
// 
#define	register_ReadBit(src, bit_no)	\
	(((src) >> (bit_no)) & 1)

#if	register_Shorthand >= 1
#define	regReadBit		register_ReadBit
#endif

#if	register_Shorthand >= 2
#define	ReadBit			register_ReadBit
#endif



// Function:
// SetBit(dst, <list of bit_no to set>)
// 
// Sets (to 1) the bits at specified bit numbers (<list of bit_no to set>)
// of the specified destination register.
// 
// Parameters:
// dst:						the destination register
// <list of bit_no to set>:	the index of the bits to set (starts from 0)
// 
// Returns:
// nothing
// 
#define	register_SetBit1(dst, bit_no)	\
	((dst) |= (1 << (bit_no)))

#define	register_SetBit2(dst, bit_no1, bit_no0)	\
	((dst) |= (1 << (bit_no1)) | (1 << (bit_no0)))

#define	register_SetBit3(dst, bit_no2, bit_no1, bit_no0)	\
	((dst) |= (1 << (bit_no2)) | (1 << (bit_no1)) | (1 << (bit_no0)))

#define	register_SetBit4(dst, bit_no3, bit_no2, bit_no1, bit_no0)	\
	((dst) |= (1 << (bit_no3)) | (1 << (bit_no2)) | (1 << (bit_no1)) | (1 << (bit_no0)))

#define	register_SetBit5(dst, bit_no4, bit_no3, bit_no2, bit_no1, bit_no0)	\
	((dst) |= (1 << (bit_no4)) | (1 << (bit_no3)) | (1 << (bit_no2)) | (1 << (bit_no1)) | (1 << (bit_no0)))

#define	register_SetBit6(dst, bit_no5, bit_no4, bit_no3, bit_no2, bit_no1, bit_no0)	\
	((dst) |= (1 << (bit_no5)) | (1 << (bit_no4)) | (1 << (bit_no3)) | (1 << (bit_no2)) | (1 << (bit_no1)) | (1 << (bit_no0)))

#define	register_SetBit7(dst, bit_no6, bit_no5, bit_no4, bit_no3, bit_no2, bit_no1, bit_no0)	\
	((dst) |= (1 << (bit_no6)) | (1 << (bit_no5)) | (1 << (bit_no4)) | (1 << (bit_no3)) | (1 << (bit_no2)) | (1 << (bit_no1)) | (1 << (bit_no0)))

#define	register_SetBit(...)	\
	Macro(Macro8(__VA_ARGS__, register_SetBit7, register_SetBit6, register_SetBit5, register_SetBit4, register_SetBit3, register_SetBit2, register_SetBit1)(__VA_ARGS__))

#if	register_Shorthand >= 1
#define	regSetBit			register_SetBit
#endif

#if	register_Shorthand >= 2
#define	SetBit				register_SetBit
#endif



// Function:
// ClearBit(dst, <list of bit_no to clear>)
// 
// Clears (to 0) the bits at specified bit numbers (<list of bit_no to clear>)
// of the specified destination register.
// 
// Parameters:
// dst:						the destination register
// <list of bit_no to set>:	the index of the bits to clear (starts from 0)
// 
// Returns:
// nothing
// 
#define	register_ClearBit1(dst, bit_no)	\
	((dst) &= ~(1 << (bit_no)))

#define	register_ClearBit2(dst, bit_no1, bit_no0)	\
	((dst) &= ~(1 << (bit_no1)) | (1 << (bit_no0)))

#define	register_ClearBit3(dst, bit_no2, bit_no1, bit_no0)	\
	((dst) &= ~(1 << (bit_no2)) | (1 << (bit_no1)) | (1 << (bit_no0)))

#define	register_ClearBit4(dst, bit_no3, bit_no2, bit_no1, bit_no0)	\
	((dst) &= ~(1 << (bit_no3)) | (1 << (bit_no2)) | (1 << (bit_no1)) | (1 << (bit_no0)))

#define	register_ClearBit5(dst, bit_no4, bit_no3, bit_no2, bit_no1, bit_no0)	\
	((dst) &= ~(1 << (bit_no4)) | (1 << (bit_no3)) | (1 << (bit_no2)) | (1 << (bit_no1)) | (1 << bit_no0))

#define	register_ClearBit6(dst, bit_no5, bit_no4, bit_no3, bit_no2, bit_no1, bit_no0)	\
	((dst) &= ~(1 << (bit_no5)) | (1 << (bit_no4)) | (1 << (bit_no3)) | (1 << (bit_no2)) | (1 << (bit_no1)) | (1 << (bit_no0)))

#define	register_ClearBit7(dst, bit_no6, bit_no5, bit_no4, bit_no3, bit_no2, bit_no1, bit_no0)	\
	((dst) &= ~(1 << (bit_no6)) | (1 << (bit_no5)) | (1 << (bit_no4)) | (1 << (bit_no3)) | (1 << (bit_no2)) | (1 << (bit_no1)) | (1 << (bit_no0)))

#define	register_ClearBit(...)	\
	Macro(Macro8(__VA_ARGS__, register_ClearBit7, register_ClearBit6, register_ClearBit5, register_ClearBit4, register_ClearBit3, register_ClearBit2, register_ClearBit1)(__VA_ARGS__))

#if	register_Shorthand >= 1
#define	regClearBit			register_ClearBit
#endif

#if	register_Shorthand >= 2
#define	ClearBit			register_ClearBit
#endif



// Function:
// WriteBit(dst, bit_no, bit_value)
// 
// Modifies (to bit_value) the bit at specified bit number (bit_no)
// of the specified destination register.
// 
// Parameters:
// dst:			the destination register
// bit_no:		the index of the bit (starts from 0)
// bit_value:	the value of the specified bit (0 or 1)
// 
// Returns:
// nothing
// 
#define	register_WriteBit(dst, bit_no, bit_value)	\
	((dst) = ((dst) & ~(1 << (bit_no))) | ((bit_value) << (bit_no)))

#if	register_Shorthand >= 1
#define	regWriteBit			register_WriteBit
#endif

#if	register_Shorthand >= 2
#define	WriteBit			register_WriteBit
#endif



#endif
