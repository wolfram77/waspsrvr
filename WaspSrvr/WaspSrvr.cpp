#include "stdafx.h"
#include "WaspSrvr.h"

void waspPropTest();
void expIntTest();

int _tmain(int argc, _TCHAR* argv[])
{
	waspPropTest();
	expIntTest();
	socketInit();
	printf("Winsock initialized.\n");

	printf("\n\n");
	system("PAUSE");
	return 0;
}

void waspPropTest()
{
	HANDLE memRgn = memNewRegion(0, 0);
	waspprops* props = wprpNew(memRgn);
	for(int i=0; i<1000; i++)
	{
		wprpSet(props, 4*i+2, "Rony", 5);
		wprpSet(props, 4*i+3, "Rinku", 6);
		wprpSet(props, 4*i+0, "Baba", 5);
		wprpSet(props, 4*i+1, "Mama", 5);
	}
	for(int i=0; i<900; i++)
	{
		wprpRemove(props, 4*i+2);
		wprpRemove(props, 4*i+3);
		wprpRemove(props, 4*i+0);
		wprpRemove(props, 4*i+1);
	}
	printf("10. %s\n", props->prop[wprpRead(props, 4*900+1)].value);
	HANDLE file = CreateFile(_T("props.txt"), (GENERIC_READ | GENERIC_WRITE), FILE_SHARE_READ, null, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, null);
	wprpStore(file, props);
	wprpFree(props);
	props = wprpFetch(memRgn, file);
	CloseHandle(file);
	printf("%s, %s, %s, %s\n", props->prop[0].value, props->prop[1].value, props->prop[2].value, props->prop[3].value);
	printf("20. %s\n", props->prop[wprpRead(props, 4*900+2)].value);
	memDeleteRegion(memRgn);
}

void expIntTest()
{
	byte x[16];
	exi val = 23;
	printf("test: ExpInt\n");
	exiWrite(x, 0, val, 15);
	val = 0;
	val = exiRead(x, 0);
}
