/*
----------------------------------------------------------------------------------------
	Type: Type definition and conversion module
	File: Type.h

	This file is part of WaspSrvr. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WaspSrvr is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WaspSrvr is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WaspSrvr.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/



/*
	Type is a type definition and conversion module for WaspSrvr. It has been developed for
	for being used in the development of the internet of things. For information,
	on its usage, please visit: https://github.com/wolfram77/WaspSrvr.
*/



#ifndef	_Type_h_
#define	_Type_h_



// Requisite headers
#include "Macro.h"



// Select shorthand level
// 
// The default shorthand level is 2 i.e., members of this
// module can be accessed as <function_name> directly.
// The shorthand level can be selected in the main header
// file of WaspSrvr library
#ifndef	type_Shorthand
#define	type_Shorthand	2
#endif



// shorthand constants
#ifndef null
#define null	0
#endif
#ifndef	NULL
#define	NULL	0
#endif
#ifndef TRUE
#define TRUE	1
#define	FALSE	0
#endif



// shorthand data-types
#ifndef byte
typedef	unsigned char		byte;
#endif
typedef signed char			sbyte;
typedef unsigned short		ushort;
typedef unsigned int		uint;
typedef unsigned long		ulong;
typedef	signed char			int8;
typedef	unsigned char		uint8;
typedef	short				int16;
typedef	unsigned short		uint16;
typedef	long				int32;
typedef	unsigned long		uint32;
typedef	long long			int64;
typedef	unsigned long long	uint64;
typedef char*				string;



// Type Mold format
// 
// Type objects can be created with different sizes. type_Mold16 has a size of 16 bytes,
// type_Mold32 has 32 bytes size, and so on. The range is from 16 to 256 bytes (in powers
// of 2). Type molds of other sizes can be made using MoldMake(size). The default Mold has
// a size of 16 bytes.
// 
#define	type_MoldMakeIntComm(size)	\
	byte	Byte[size];	\
	sbyte	Sbyte[16];	\
	char	Char[16];	\
	short	Short[8];	\
	ushort	Ushort[8];	\
	long	Long[4];	\
	ulong	Ulong[4];	\
	int8	Int8[16];	\
	uint8	Uint8[16];	\
	int16	Int16[8];	\
	uint16	Uint16[8];	\
	int32	Int32[4];	\
	uint32	Uint32[4];	\
	int64	Int64[2];	\
	uint64	Uint64[2];	\
	float	Float[4];	\
	double	Double[2]

#if WaspSrvr_WordSize > 16
#define	type_MoldMakeInt(size)	\
	type_MoldMakeIntComm(size);	\
	int		Int[4];		\
	uint	Uint[4]
#else
#define	type_MoldMakeInt(size)	\
	type_MoldMakeIntComm(size);	\
	int		Int[8];		\
	uint	Uint[8]
#endif

#if	type_Shorthand == 0
#define	type_MoldMake(size)	\
	typedef union _type_Mold##size	\
	{	\
		type_MoldMakeInt(size);	\
	}type_Mold##size
#elif type_Shorthand == 1
#define	type_MoldMake(size)	\
	typedef union _type_Mold##size	\
	{	\
		type_MoldMakeInt(size);	\
	}type_Mold##size, typMold##size
#elif type_Shorthand >= 2
#define	type_MoldMake(size)	\
	typedef union _type_Mold##size	\
	{	\
		type_MoldMakeInt(size);	\
	}type_Mold##size, typMold##size, Mold##size
#endif

type_MoldMake(16);

type_MoldMake(32);

type_MoldMake(64);

type_MoldMake(128);

type_MoldMake(256);

#ifndef	type_Mold
#define	type_Mold			type_Mold16
#endif

#if type_Shorthand >= 1
#define	typMoldMake		type_MoldMake
#define	typMold			type_Mold
#endif

#if	type_Shorthand >= 2
#define	MoldMake		type_MoldMake
#define	Mold			type_Mold
#endif



// Internal Type object
// 
// WaspSrvr.Type has an internal type object used for type conversions. It can be used
// through functions provided in this library, and can also be accessed manually
// as "TypeObj".
// 
type_Mold	TypeObj;



// Function:
// To<Type>(smaller_data_types)
// 
// Assembles smaller data types to a bigger data type. The
// assembling is done in little endian format, which means
// that the smaller data representing the least significant
// part should come first, and the smaller data representing
// the most significant part should come last.
// 
// Parameters:
// smaller_data_types:	list of bytes, shorts, ints, etc.
// 
// Returns:
// <type>_value:	the value of the (bigger) assembled data type
// 
#define	type_ToNibble(bit3, bit2, bit1, bit0)	\
	((byte)(((bit3) << 3) | ((bit2) << 2) | ((bit1) << 1) | (bit0)))

#define type_ToByteNib(nibble1, nibble0)	\
	((byte)(((nibble1) << 4) | (nibble0)))

#define	type_ToByteBit(bit7, bit6, bit5, bit4, bit3, bit2, bit1, bit0)	\
	type_ToByteNib(type_ToNibble(bit7, bit6, bit5, bit4), type_ToNibble(bit3, bit2, bit1, bit0))

#define type_ToByte(...)	\
	Macro(Macro8(__VA_ARGS__, type_ToByteBit, _7, _6, _5, _4, _3, type_ToByteNib)(__VA_ARGS__))

#define	type_ToSbyte(...)	\
	Macro((sbyte)type_ToByte(__VA_ARGS__))

#define	type_ToInt8	\
	type_ToSbyte

#define	type_ToUint8	\
	type_ToByte

#define	type_ToChar(...)	\
	Macro((char)type_ToByte(__VA_ARGS__))

#define	type_ToType2(var, ret, rtype, dat1, dat0)	\
	(((TypeObj.var[0] = (dat0)) & (TypeObj.var[1] = (dat1)) & 0)? (rtype)0 : TypeObj.ret[0])

#define	type_ToType4(var, ret, rtype, dat3, dat2, dat1, dat0)	\
	(((TypeObj.var[0] = (dat0)) & (TypeObj.var[1] = (dat1)) & (TypeObj.var[2] = (dat2)) & (TypeObj.var[3] = (dat3)) & 0)? (rtype)0 : TypeObj.ret[0])

#define type_ToType8(var, ret, rtype, dat7, dat6, dat5, dat4, dat3, dat2, dat1, dat0)	\
	(((TypeObj.var[0] = (dat0)) & (TypeObj.var[1] = (dat1)) & (TypeObj.var[2] = (dat2)) & (TypeObj.var[3] = (dat3)) & (TypeObj.var[4] = (dat4)) & (TypeObj.var[5] = (dat5)) & (TypeObj.var[6] = (dat6)) & (TypeObj.var[7] = (dat7)) & 0)? (rtype)0 : TypeObj.ret[0])

#define	type_ToShort(byte1, byte0)	\
	((short)(((byte1) << 8) | byte0))

#define	type_ToUshort(byte1, byte0)	\
	((ushort)(((byte1) << 8) | byte0))

#define	type_ToInt16	\
	type_ToShort

#define	type_ToUint16	\
	type_ToUshort

#define	type_ToInt32Srt(ushort1, ushort0)	\
	type_ToType2(Ushort, Int32, int32, ushort1, ushort0)

#define	type_ToInt32Byt(byte3, byte2, byte1, byte0)	\
	type_ToType2(Byte, Int32, int32, byte3, byte2, byte1, byte0)

#define	type_ToInt32(...)	\
	Macro(Macro4(__VA_ARGS__, type_ToInt32Byt, _3, type_ToInt32Srt)(__VA_ARGS__))

#define	type_ToUint32Srt(ushort1, ushort0)	\
	type_ToType2(Ushort, Uint32, uint32, ushort1, ushort0)

#define	type_ToUint32Byt(byte3, byte2, byte1, byte0)	\
	type_ToType2(Byte, Uint32, uint32, byte3, byte2, byte1, byte0)

#define	type_ToUint32(...)	\
	Macro(Macro4(__VA_ARGS__, type_ToUint32Byt, _3, type_ToUint32Srt)(__VA_ARGS__))

#define	type_ToLong	\
	type_ToInt32

#define	type_ToUlong	\
	type_ToUint32

#define	type_ToInt64Lng(ulong1, ulong0)	\
	type_ToType2(Ulong, Int64, int64, ulong1, ulong0)

#define	type_ToInt64Srt(ushort3, ushort2, ushort1, ushort0)	\
	type_ToType4(Ushort, Int64, int64, ushort3, ushort2, ushort1, ushort0)

#define	type_ToInt64Byt(byte7, byte6, byte5, byte4, byte3, byte2, byte1, byte0)	\
	type_ToType8(Byte, Int64, int64, byte7, byte6, byte5, byte4, byte3, byte2, byte1, byte0)

#define	type_ToInt64(...)	\
	Macro(Macro8(__VA_ARGS__, type_ToInt64Byt, _7, _6, _5, type_ToInt64Srt, _3, type_ToInt64Lng)(__VA_ARGS__))

#define	type_ToUint64Lng(ulong1, ulong0)	\
	type_ToType2(Ulong, Uint64, uint64, ulong1, ulong0)

#define	type_ToUint64Srt(ushort3, ushort2, ushort1, ushort0)	\
	type_ToType4(Ushort, Uint64, uint64, ushort3, ushort2, ushort1, ushort0)

#define	type_ToUint64Byt(byte7, byte6, byte5, byte4, byte3, byte2, byte1, byte0)	\
	type_ToType8(Byte, Uint64, uint64, byte7, byte6, byte5, byte4, byte3, byte2, byte1, byte0)

#define	type_ToUint64(...)	\
	Macro(Macro8(__VA_ARGS__, type_ToUint64Byt, _7, _6, _5, type_ToUint64Srt, _3, type_ToUint64Lng)(__VA_ARGS__))

#if WaspSrvr_WordSize > 16
#define	type_ToInt	\
	type_ToInt32

#define	type_ToUint	\
	type_ToUint32
#else
#define	type_ToInt	\
	type_ToInt16

#define	type_ToUint	\
	type_ToUint16
#endif

#define	type_ToFloatSrt(ushort1, ushort0)	\
	type_ToType2(Ushort, Float, float, ushort1, ushort0)

#define	type_ToFloatByt(byte3, byte2, byte1, byte0)	\
	type_ToType4(Byte, Float, float, byte3, byte2, byte1, byte0)

#define	type_ToFloat(...)	\
	Macro(Macro4(__VA_ARGS__, type_ToFloatByt, _3, type_ToFloatSrt)(__VA_ARGS__))

#define	type_ToDoubleUlong32(ulong32_1, ulong32_0)	\
	type_ToType2(Uint, Double, double, ulong32_1, ulong32_0)

#define	type_ToDoubleSrt(ushort3, ushort2, ushort1, ushort0)	\
	type_ToType4(Ushort, Double, double, ushort3, ushort2, ushort1, ushort0)

#define	type_ToDoubleByt(byte7, byte6, byte5, byte4, byte3, byte2, byte1, byte0)	\
	type_ToType8(Byte, Double, double, byte7, byte6, byte5, byte4, byte3, byte2, byte1, byte0)

#define	type_ToDouble(...)	\
	Macro(Macro8(__VA_ARGS__, type_ToDoubleByt, _7, _6, _5, type_ToDoubleSrt, _3, type_ToDoubleUlong32)(__VA_ARGS__))

#if	type_Shorthand >= 1
#define	typToNibble		type_ToNibble
#define	typToByte		type_ToByte
#define	typToSbyte		type_ToSbyte
#define	typToChar		type_ToChar
#define	typToShort		type_ToShort
#define	typToUshort		type_ToUshort
#define	typToInt		type_ToInt
#define	typToUint		type_ToUint
#define	typToLong		type_ToLong
#define	typToUlong		type_ToUlong
#define	typToInt8		type_ToInt8
#define	typToUint8		type_ToUint8
#define	typToInt16		type_ToInt16
#define	typToUint16		type_ToUint16
#define	typToInt32		type_ToInt32
#define	typToUint32		type_ToUint32
#define	typToInt64		type_ToInt64
#define	typToUint64		type_ToUint64
#define	typToFloat		type_ToFloat
#define	typToDouble		type_ToDouble
#endif

#if	type_Shorthand >= 2
#define	ToNibble		type_ToNibble
#define	ToByte			type_ToByte
#define	ToSbyte			type_ToSbyte
#define	ToChar			type_ToChar
#define	ToShort			type_ToShort
#define	ToUshort		type_ToUshort
#define	ToInt			type_ToInt
#define	ToUint			type_ToUint
#define	ToLong			type_ToLong
#define	ToUlong			type_ToUlong
#define	ToInt8			type_ToInt8
#define	ToUint8			type_ToUint8
#define	ToInt16			type_ToInt16
#define	ToUint16		type_ToUint16
#define	ToInt32			type_ToInt32
#define	ToUint32		type_ToUint32
#define	ToInt64			type_ToInt64
#define	ToUint64		type_ToUint64
#define	ToFloat			type_ToFloat
#define	ToDouble		type_ToDouble
#endif



// Function:
// BinToHex(*dst, sz, *src, off, len, opt)
// 
// Read hexadecimal string (dst) of maximum specified size (sz) of
// the soure binary data (src + off) of specified length (len). The
// options (opt) specify how the conversion is to be performed, and
// it takes as input a set of flags. If source base address is not
// specified, this library's internal buffer is assumed as the source
// base address.
// 
// Parameters:
// dst:	      the destination string where hex string will be stored
// sz:        the maximum possible size of the hex string (buffer size)
// src:	      the base address of source binary data
// off:	      offset to the binary data to be converted (src + off)
// len:	      length of data to be converted
// opt:	      conversion options (TYPE_ADD_SPACE, TYPE_ADD_CHAR, TYPE_BIG_ENDIAN)
// 
// Returns:
// nothing
// 
#define type_HEX_TO_BIN(ch)		(((ch) <= '9')? (ch)-'0' : (ch)-'7')

#define type_BIN_TO_HEX(bn)		(((bn) <= 9)? (bn)+'0' : (bn)+'7' )

#define	type_NO_SPACE			0

#define type_ADD_SPACE			1

#define	type_HAS_SPACE			1

#define	type_NO_CHAR			0

#define type_ADD_CHAR			2

#define	type_HAS_CHAR			2

#define	type_LITTLE_ENDIAN		0

#define type_BIG_ENDIAN			4

string type_BinToHexS(string dst, int sz, byte* src, int len, byte opt)
{
	string dend = dst + (sz - 1);
	src += (opt & type_BIG_ENDIAN)? 0 : (len - 1);
	int stp = (opt & type_BIG_ENDIAN)? 1 : -1;
	for(int i=0; i<len; i++, src+=stp)
	{
		*dst = type_BIN_TO_HEX(*src >> 4); dst++; if(dst >= dend) break;
		*dst = type_BIN_TO_HEX(*src & 0xF); dst++; if(dst >= dend) break;
		if(opt & type_ADD_CHAR) { *dst = (*src < 32 || *src > 127)? '.' : *src; dst++; if(dst >= dend) break;}
		if(opt & type_ADD_SPACE) { *dst = ' '; dst++; if(dst >= dend) break;}
	}
	*dst = '\0';
	return dst;
}

#define	type_BinToHex(dst, sz, src, off, len, opt)	\
	type_BinToHexS((string)(dst), (int)(sz), (byte*)(src) + (off), (int)(len), (byte)(opt))

#if type_Shorthand >= 1
#define	typHEX_TO_BIN			type_HEX_TO_BIN
#define	typBIN_TO_HEX			type_BIN_TO_HEX
#define	typNO_SPACE				type_NO_SPACE
#define	typADD_SPACE			type_ADD_SPACE
#define	typHAS_SPACE			type_HAS_SPACE
#define	typNO_CHAR				type_NO_CHAR
#define	typADD_CHAR				type_ADD_CHAR
#define	typHAS_CHAR				type_HAS_CHAR
#define	typLITTLE_ENDIAN		type_LITTLE_ENDIAN
#define	typBIG_ENDIAN			type_BIG_ENDIAN
#define	typBinToHex				type_BinToHex
#endif

#if	type_Shorthand >= 2
#define	HEX_TO_BIN			type_HEX_TO_BIN
#define	BIN_TO_HEX			type_BIN_TO_HEX
#define	NO_SPACE			type_NO_SPACE
#define	ADD_SPACE			type_ADD_SPACE
#define	HAS_SPACE			type_HAS_SPACE
#define	NO_CHAR				type_NO_CHAR
#define	ADD_CHAR			type_ADD_CHAR
#define	HAS_CHAR			type_HAS_CHAR
#define	LITTLE_ENDIAN		type_LITTLE_ENDIAN
#define	BIG_ENDIAN			type_BIG_ENDIAN
#define	BinToHex			type_BinToHex
#endif



// Function:
// HexToBin(*dst, off, len, *src, opt)
// 
// Writes binary data from the source hex string (src) to the destination
// address (dst + off) of specified length len. The options (opt) specify
// how the conversion is to be performed, and it takes as input a set of
// flags. If destination base address is not specified, this library's
// internal buffer is assumed as the destination base address.
// 
// Parameters:
// dst:	      the base address of destination
// off:	      the destination offset where the binary data will be stored (dst + off)
// len:       length of data at destination
// src:	      the hex string to be converted
// opt:	      conversion options (TYPE_HAS_SPACE, TYPE_HAS_CHAR, TYPE_BIG_ENDIAN)
//
// Returns:
// the converted data (dst)
// 
void type_HexToBinD(byte* dst, int len, string src, byte opt)
{
	char* psrc = src + strlen(src) - 1;
	dst += (opt & type_BIG_ENDIAN)? (len - 1) : 0;
	int i ,stp = (opt & type_BIG_ENDIAN)? -1 : 1;
	for(i=0; i<len; i++, dst+=stp)
	{
		if(opt & type_HAS_SPACE) psrc--;
		if(opt & type_HAS_CHAR) psrc--;
		*dst = (psrc < src)? 0 : type_HEX_TO_BIN(*psrc); psrc--;
		*dst |= (psrc < src)? 0 : type_HEX_TO_BIN(*psrc) << 4; psrc--;
	}
}

#define	type_HexToBin(dst, off, len, src, opt)	\
	type_HexToBinD((byte*)(dst) + (off), (int)(len), (string)(src), (byte)(opt))

#if	type_Shorthand >= 1
#define	typHexToBin		type_HexToBin
#endif

#if	type_Shorthand >= 2
#define	HexToBin		type_HexToBin
#endif



#endif
