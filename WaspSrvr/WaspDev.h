/*
----------------------------------------------------------------------------------------
	WaspDev: WASP Device operations module
	File: WaspDev.h

	This file is part of WaspSrvr. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WaspSrvr is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WaspSrvr is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WaspSrvr.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/



/*
	WaspDev is a WASP device operations module for WaspSrvr. It has been developed for
	for being used in the development of the internet of things. For information,
	on its usage, please visit: https://github.com/wolfram77/WaspSrvr.
*/



#ifndef	_WaspDev_h_
#define	_WaspDev_h_



// Requisite headers
#include "Macro.h"
#include "Type.h"
#include <Windows.h>



// Select shorthand level
// 
// The default shorthand level is 1 i.e., members of this
// module can be accessed as wdev<function_name> directly.
// The shorthand level can be selected in the main header
// file of WaspSrvr library
#ifndef	waspdev_Shorthand
#define	waspdev_Shorthand	1
#endif



// WASP Device directory and files
string waspdev_RootDir = "\\data\\dev\\";
string waspdev_Props = "\\props";
string waspdev_ExProps = "\\exprops";
string waspdev_DataList = "\\data_list";
string waspdev_Data = "\\data";

#if waspdev_Shorthand >= 1
#define	wdevRootDir		waspdev_RootDir
#define	wdevProps		waspdev_Props
#define	wdevExProps		waspdev_ExProps
#define	wdevDataList	waspdev_DataList
#define	wdevData		waspdev_Data
#endif

#if waspdev_Shorthand >= 2
#define	RootDir		waspdev_RootDir
#define	Props		waspdev_Props
#define	ExProps		waspdev_ExProps
#define	DataList	waspdev_DataList
#define	Data		waspdev_Data
#endif



// WASP Properties structure
// [uint64: Reserved]
// [byte: Properties Type]
// [byte: Device Type]
// ...
typedef struct _waspdev_DevId
{
	uint64	val[2];
}waspdev_DevId;

typedef struct _waspdev_WaspSrvrProps
{
	uint64	id;
	uint32	type;
	double	geoLat;
	double	geoLong;
}waspdev_WaspSrvrProps;

typedef struct _waspdev_WebSrvrProps
{
	uint64	id;
	uint32	type;
	double	geoLat;
	double	geoLong;
}waspdev_WebSrvrProps;

typedef struct _waspdev_GatewayProps
{
	uint64	id;
	uint32	type;
	double	geoLat;
	double	geoLong;
}waspdev_GatewayProps;

typedef struct _waspdev_DeviceProps
{
	uint64	id;
	uint32	type;
	double	geoLat;
	double	geoLong;
}waspdev_DeviceProps;

typedef struct _waspdev_SensorProps
{
	uint64	id;
	uint32	type;
	double	geoLat;
	double	geoLong;
}waspdev_SensorProps;

typedef struct _waspdev_ActuatorProps
{
	uint64	id;
	uint32	type;
	double	geoLat;
	double	geoLong;
}waspdev_ActuatorProps;



// Function:
// SetBit(dst, <list of bit_no to set>)
// 
// Sets (to 1) the bits at specified bit numbers (<list of bit_no to set>)
// of the specified destination waspdev.
// 
// Parameters:
// dst:						the destination waspdev
// <list of bit_no to set>:	the index of the bits to set (starts from 0)
// 
// Returns:
// nothing
// 
void waspdev_AddDev(waspdev_DevId id, int category)
{
	// get hex name of the device

	// first check whether the device already exists
}


void waspdev_WriteData(void* src, int size)
{
}


#endif
