/*
----------------------------------------------------------------------------------------------------
	WaspSrvr: Wirelessly Attached Server Protocol - Server for the Internet of Things (IoT) (C/C++)
	File: WaspSrvr.h

	This file is part of WaspSrvr. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WaspSrvr is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WaspSrvr is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WaspSrvr.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------------------
*/



/*
	WaspSrvr is WASP server program for managing sensors for the Internet pf Things. To use it, modify
	the appropriate definitions in this header file. This header should already include all te requisite
	header files (including those which are which are part of this library). For any information, please
	visit: https://github.com/wolfram77/WaspSrvr.
*/



#ifndef	_WaspSrvr_h_
#define	_WaspSrvr_h_



// Include required headers
#include "WaspInc.h"



// PC global data
#if lib_Platform == lib_PlatformPC
WSADATA		wsaData;
#endif



// PC platform code
#if lib_Platform == lib_PlatformPC
// initializes winsock
inline bool socketInit()
{
	int iRes = WSAStartup(MAKEWORD(2,2), &wsaData);
	if(iRes != 0)
	{
		printf("Winsock initialization failed!\n\n");
		return true;
	}
	return false;
}
#endif



// PC platform WASP Server setup class
#if lib_Platform == lib_PlatformPC
class WaspSrvr
{
	// properties
public:


public:
	// start a new WASP server on a specific port
	WaspSrvr(int port);

	// terminate a running WASP server
	~WaspSrvr();
};
#endif



#endif // !_WaspSrvr_h_
