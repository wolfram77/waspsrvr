/*
----------------------------------------------------------------------------------------
	ExpInt: Expandable Integer module
	File: ExpInt.h

	This file is part of WaspSrvr. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WaspSrvr is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WaspSrvr is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WaspSrvr.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/



/*
	ExpInt is an expandable integer support module for WaspSrvr. It has been developed for
	for being used in the development of the internet of things. For information,
	on its usage, please visit: https://github.com/wolfram77/WaspSrvr.
*/



#ifndef	_ExpInt_h_
#define	_ExpInt_h_



// Format of Expandable integer
// 
// -------------------------------------
// | byte 0 | byte 1 |  ...   | byte n |
// -------------------------------------
// The integer is stored in little endian format. The number of least significant trailing '1' bits indicate
// the size of the expandable number in multiples of 7 bits. These trailing '1' bits is followed by a '0' bit
// which marks the end of integer size count and start of value. If there are no trailing '1' s in the least
// significant side, it indicates, that the number is of 7 bits size. Size of number = (N + 1) * 7, where
// N = number of '1' trailing bits.



// Requisite headers
#include "Macro.h"
#include "Type.h"



// Select shorthand level
// 
// The default shorthand level is 1 i.e., members of this
// module can be accessed as exi<function_name> directly.
// The shorthand level can be selected in the main header
// file of WaspSrvr library
#ifndef	expint_Shorthand
#define	expint_Shorthand	1
#endif



// shorthand data-types
typedef int64		expint;
#if expint_Shorthand >= 1
typedef	expint		exi;
#endif



// properties
uint	expint_BitSize;
#if expint_Shorthand >= 1
#define	exiBitSize		expint_BitSize
#endif



// Function:
// Read(*src, off)
// 
// Reads an expandable integer from specified source address
// 
// Parameters:
// src:		source base address
// off:		offset of expandable integer from base address
// 
// Returns:
// value:		the value of expandable integer as int64
// (BitSize:	size of the expandable integer in bits)
// 
int64 expint_ReadS(void* src)
{
	expint_BitSize = 7;
	int64 sel = 0x80;
	int64 val = *((int64*)src);
	while(val & 1)
	{
		expint_BitSize += 7;
		sel <<= 7;
		val >>= 1;
	}
	sel -= 1;
	val = (val >> 1) & sel;
	return val;
}

#define	expint_Read(src, off)	\
	expint_ReadS((byte*)(src) + (off))

#if	expint_Shorthand >= 1
#define	exiRead		expint_Read
#endif

#if	expint_Shorthand >= 2
#define	Read		expint_Read
#endif



// Function:
// Write(*dst, off, val, bitsz)
// 
// Writes an expandable integer to specified destination address
// 
// Parameters:
// dst:		destination base address
// off:		offset of expandable integer from base address
// val:		the value of expandable integer as int64
// bitsz:	bit size of value
// 
// Returns:
// bytesz:	byte size of value in destination
// 
int expint_WriteD(void* dst, int64 val, int bitsz)
{
	int bytesz = 1;
	val <<= 1;
	bitsz -= 7;
	while(bitsz > 0)
	{
		val = (val << 1) | 1;
		bitsz -= 7;
		bytesz += 1;
	}
	*((int64*)dst) = val;
	return bytesz;
}

#define	expint_Write(dst, off, val, bitsz)	\
	expint_WriteD((byte*)(dst) + (off), val, bitsz)

#if	expint_Shorthand >= 1
#define	exiWrite	expint_Write
#endif

#if	expint_Shorthand >= 2
#define	Write		expint_Write
#endif



#endif
