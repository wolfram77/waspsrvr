/*
----------------------------------------------------------------------------------------
	Search: Array element search module
	File: Search.h

	This file is part of WaspSrvr. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WaspSrvr is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WaspSrvr is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WaspSrvr.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/



/*
	Search is an array element search module for WaspSrvr. It has been developed for
	for being used in the development of the internet of things. For information,
	on its usage, please visit: https://github.com/wolfram77/WaspSrvr.
*/



#ifndef	_Search_h_
#define	_Search_h_



// Requisite headers
#include "Macro.h"
#include "Type.h"



// Select shorthand level
// 
// The default shorthand level is 1 i.e., members of this
// module can be accessed as sch<function_name> directly.
// The shorthand level can be selected in the main header
// file of WaspSrvr library
#ifndef	search_Shorthand
#define	search_Shorthand	1
#endif



// Function:
// <Type>Sorted(src, off, len, elem)
// 
// Searches for a given element in a sorted array (binary search)
// 
// Parameters:
// src:		source base address
// off:		offset to the start of array
// len:		length of the array
// elem:	element to search for
// 
// Returns:
// indx:	index of the element in array (-1 = not found)
// 
void search_IntSorted(int* src, int len, int elem)
{
}



#endif
