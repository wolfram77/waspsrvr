/*
----------------------------------------------------------------------------------------
	WaspProp: WASP Device Properties operations module
	File: WaspProp.h

	This file is part of WaspSrvr. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WaspSrvr is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WaspSrvr is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WaspSrvr.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/



/*
	WaspProp is a WASP device properties operations module for WaspSrvr. It has been developed for
	for being used in the development of the internet of things. For information,
	on its usage, please visit: https://github.com/wolfram77/WaspSrvr.
*/



#ifndef	_WaspProp_h_
#define	_WaspProp_h_



// Format of WASP Properties
// 
// [<32bit> number of Properties]
// [(sorted)<16bit> Property type][<16bit> Ptr to Property size][<32bit> Ptr to Property value]
// [<nbit> Property data]



// Requisite headers
#include "Macro.h"
#include "Type.h"
#include <Windows.h>



// Select shorthand level
// 
// The default shorthand level is 1 i.e., members of this
// module can be accessed as wprp<function_name> directly.
// The shorthand level can be selected in the main header
// file of WaspSrvr library
#ifndef	waspprop_Shorthand
#define	waspprop_Shorthand	1
#endif



// shorthand data-types
#define	waspprop_MAX_PROPS			64
#define	waspprop_PROPS_BUFFERING	64

#if waspprop_Shorthand >= 1
#define	wprpMAX_PROPS		waspprop_MAX_PROPS
#endif

#if waspprop_Shorthand >= 2
#define	MAX_PROPS		waspprop_MAX_PROPS
#endif

typedef struct _waspprop
{
	uint16	type;
	uint16	size;
	void*	value;
}waspprop;

typedef struct _waspprops_hdr
{
	HANDLE		memRgn;
	void*		defData;
	int32		allocCount;
	int32		count;
}waspprops_hdr;

typedef struct _waspprops
{
	HANDLE		memRgn;
	void*		defData;
	uint32		allocCount;
	uint32		count;
	waspprop	prop[waspprop_MAX_PROPS];
}waspprops;

#if waspprop_Shorthand >= 1
typedef	waspprop		wprp;
typedef	waspprops_hdr	wprps_hdr;
typedef waspprops		wprps;
#endif



// Function:
// Fetch(mem_rgn, file)
// 
// Fetches properties from a property file to memory
// 
// Parameters:
// mem_rgn:	memory region in which to allocate
// file:	file handle of property file
// 
// Returns:
// *props:	pointer to properties loaded into memory
// (memory allocated from waspprop's set memory region)
// 
waspprops* waspprop_Fetch(HANDLE mem_rgn, HANDLE file)
{
	BOOL ret;
	waspprops* props;
	DWORD i, count = 0, fileSize, bytesToRead, allocCount, bytesToAlloc, bytesRead;
	fileSize = SetFilePointer(file, 0, null, FILE_END);
	ret = SetFilePointer(file, 0, null, FILE_BEGIN);
	if(ret != 0) return null;
	ret = ReadFile(file, &count, sizeof(uint32), &bytesRead, null);
	if(!ret || bytesRead != sizeof(uint32)) return null;
	allocCount = count + waspprop_PROPS_BUFFERING;
	bytesToAlloc = sizeof(waspprops_hdr) + allocCount * sizeof(waspprop);
	props = (waspprops*) memory_Alloc(mem_rgn, bytesToAlloc);
	props->memRgn = mem_rgn;
	props->allocCount = allocCount;
	props->defData = null;
	props->count = count;
	bytesToRead = count * sizeof(waspprop);
	ret = ReadFile(file, &(props->prop[0]), bytesToRead, &bytesRead, null);
	if(!ret || bytesRead != bytesToRead)
	{
		memory_Free(mem_rgn, props);
		return null;
	}
	bytesToRead = fileSize - sizeof(uint32) - bytesToRead;
	props->defData = memory_Alloc(mem_rgn, bytesToRead);
	ret = ReadFile(file, props->defData, bytesToRead, &bytesRead, null);
	if(!ret || bytesRead != bytesToRead)
	{
		memory_Free(mem_rgn, props->defData);
		memory_Free(mem_rgn, props);
		return null;
	}
	for(i = 0; i < props->count; i++)
	{
		props->prop[i].value = (byte*)(props->prop[i].value) + (uint64)(props->defData);
	}
	return props;
}

#if	waspprop_Shorthand >= 1
#define	wprpFetch		waspprop_Fetch
#endif

#if	waspprop_Shorthand >= 2
#define	Fetch		waspprop_Fetch
#endif



// Function:
// Store(file, *props)
// 
// Stores properties from memory to a property file
// 
// Parameters:
// file:	file handle of property file
// *props:	pointer to properties in memory
// 
// Returns:
// status:	true if store was successful
// 
bool waspprop_Store(HANDLE file, waspprops* props)
{
	BOOL ret;
	waspprops* temp;
	DWORD i, bytesToWrite, bytesWritten, tempSize, tempPtr;
	bytesToWrite = sizeof(uint32) + props->count * sizeof(waspprop);
	ret = SetFilePointer(file, bytesToWrite, null, FILE_BEGIN);
	if(ret != bytesToWrite) return false;
	for(i = 0; i < props->count; i++)
	{
		ret = WriteFile(file, props->prop[i].value, props->prop[i].size, &bytesWritten, null);
		if(!ret || bytesWritten < props->prop[i].size) return false;
	}
	ret = SetFilePointer(file, 0, null, FILE_BEGIN);
	if(ret != 0) return false;
	tempSize = sizeof(waspprops_hdr) + props->count * sizeof(waspprop);
	temp = (waspprops*) memAlloc(props->memRgn, tempSize);
	memCopy(temp, 0, props, 0, tempSize);
	tempPtr = 0;
	for(i = 0; i < temp->count; i++)
	{
		temp->prop[i].value = (void*)tempPtr;
		tempPtr += temp->prop[i].size;
	}
	ret = WriteFile(file, &(temp->count), bytesToWrite, &bytesWritten, null);
	memFree(props->memRgn, temp);
	if(!ret || bytesWritten != bytesToWrite) return false;
	return true;
}

#if	waspprop_Shorthand >= 1
#define	wprpStore	waspprop_Store
#endif

#if	waspprop_Shorthand >= 2
#define	Store		waspprop_Store
#endif



// Function:
// Read(*props, prop_type)
// 
// Read a property from property list by a property type
// 
// Parameters:
// *props:		pointer to properties in memory
// prop_type:	type number of the property
// 
// Returns:
// index:	index of property in property list
// (-1 for not present)
// 
int waspprop_Read(waspprops* props, uint16 prop_type)
{
	int begin = 0, end = props->count - 1, mid = 0;
	while(begin <= end)
	{
		mid = (begin + end) >> 1;
		if(prop_type < props->prop[mid].type) end = mid - 1;
		else if(prop_type > props->prop[mid].type) begin = mid + 1;
		else return mid;
	}
	return -1;
}

#if	waspprop_Shorthand >= 1
#define	wprpRead	waspprop_Read
#endif

#if	waspprop_Shorthand >= 2
#define	Read		waspprop_Read
#endif



// Function:
// Set(*props, prop_type, *prop_val, prop_sz)
// 
// Set a property to property list
// 
// Parameters:
// *props:		pointer to properties in memory
// prop_type:	type number of the property
// *prop_val:	pointer to value of property
// prop_sz:		size of property value
// 
// Returns:
// status:	true if a new property was added, false if replaced
// 
bool waspprop_Set(waspprops* &props, uint16 prop_type, void* prop_val, uint16 prop_sz)
{
	int copySz;
	int begin = 0, end = props->count - 1, mid = 0;
	while(begin <= end)
	{
		mid = (begin + end) >> 1;
		if(prop_type < props->prop[mid].type) end = mid - 1;
		else if(prop_type > props->prop[mid].type) begin = mid + 1;
		else break;
	}
	if(begin > end)
	{
		if(props->count && prop_type > props->prop[mid].type) mid++;
		if(props->count >= props->allocCount)
		{
			props->allocCount = props->count + waspprop_PROPS_BUFFERING;
			props = (waspprops*) memReAlloc(props->memRgn, props, sizeof(waspprops_hdr) + props->allocCount * sizeof(waspprop));
		}
		copySz = (props->count - mid) * sizeof(waspprop);
		if(copySz) memCopy(&(props->prop[mid+1]), 0, &(props->prop[mid]), 0, copySz);
		props->count++;
	}
	props->prop[mid].type = prop_type;
	props->prop[mid].size = prop_sz;
	props->prop[mid].value = prop_val;
	return (begin > mid);
}

#if	waspprop_Shorthand >= 1
#define	wprpSet		waspprop_Set
#endif

#if	waspprop_Shorthand >= 2
#define	Set		waspprop_Set
#endif



// Function:
// Remove(*props, prop_type)
// 
// Remove a property from property list
// 
// Parameters:
// *props:		pointer to properties in memory
// prop_type:	type number of the property
// 
// Returns:
// status:	true if a property was removed
// 
bool waspprop_Remove(waspprops* &props, uint16 prop_type)
{
	int begin = 0, end = props->count - 1, mid = 0;
	while(begin <= end)
	{
		mid = (begin + end) >> 1;
		if(prop_type < props->prop[mid].type) end = mid - 1;
		else if(prop_type > props->prop[mid].type) begin = mid + 1;
		else break;
	}
	if(begin <= end)
	{
		if(props->count <= (props->allocCount - (waspprop_PROPS_BUFFERING << 1)))
		{
			props->allocCount = props->count + waspprop_PROPS_BUFFERING;
			props = (waspprops*) memReAlloc(props->memRgn, props, sizeof(waspprops_hdr) + props->allocCount * sizeof(waspprop));
		}
		memCopy(&(props->prop[mid]), 0, &(props->prop[mid+1]), 0, (props->count - mid - 1) * sizeof(waspprop));
		props->count--;
	}
	return (begin <= mid);
}

#if	waspprop_Shorthand >= 1
#define	wprpRemove		waspprop_Remove
#endif

#if	waspprop_Shorthand >= 2
#define	Remove		waspprop_Remove
#endif



// Function:
// Free(*props)
// 
// Frees a property list from memory
// 
// Parameters:
// *props:		pointer to properties in memory
// 
// Returns:
// nothing
// 
void waspprop_Free(waspprops* props)
{
	if(props->defData) memory_Free(props->memRgn, props->defData);
	memory_Free(props->memRgn, props);
}

#if	waspprop_Shorthand >= 1
#define	wprpFree		waspprop_Free
#endif

#if	waspprop_Shorthand >= 2
#define	Free		waspprop_Free
#endif



// Function:
// New(mem_rgn)
// 
// Creates a new property list (in memory)
// 
// Parameters:
// mem_rgn:	memory region in which to allocate
// 
// Returns:
// props:	empty property list
// 
waspprops* waspprop_New(HANDLE mem_rgn)
{
	uint allocSz = sizeof(waspprops_hdr) + waspprop_PROPS_BUFFERING * sizeof(waspprop);
	waspprops* props = (waspprops*) memory_Alloc(mem_rgn, allocSz);
	props->memRgn = mem_rgn;
	props->allocCount = waspprop_PROPS_BUFFERING;
	props->defData = null;
	props->count = 0;
	return props;
}

#if	waspprop_Shorthand >= 1
#define	wprpNew		waspprop_New
#endif

#if	waspprop_Shorthand >= 2
#define	New		waspprop_New
#endif



#endif
