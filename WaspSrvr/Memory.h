/*
----------------------------------------------------------------------------------------
	Memory: Generic Memory (RAM) operations module
	File: Memory.h

	This file is part of WaspSrvr. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WaspSrvr is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WaspSrvr is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WaspSrvr.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/



/*
	Memory is a generic memory (RAM) operations module. It has been developed for
	for being used in the development of the internet of things. For information,
	on its usage, please visit: https://github.com/wolfram77/WaspSrvr.
*/



#ifndef	_Memory_h_
#define	_Memory_h_



// Requisite headers
#include "Macro.h"
#include "Type.h"



// Select shorthand level
// 
// The default shorthand level is 2 i.e., members of this
// module can be accessed as <function_name> directly.
// The shorthand level can be selected in the main header
// file of WaspSrvr library
#ifndef	memory_Shorthand
#define	memory_Shorthand	2
#endif



// Function:
// ReadBit(*src, off, bit_no)
// 
// Returns the value of bit at the specified bit number (bit_no)
// from the specified source address with offset (src + off).
// 
// Parameters:
// *src:	the base address of stored data
// off:		offset from which bit index starts
// bit_no:	the index of the bit (starts from 0)
// 
// Returns:
// value:	the value of the specified bit (0 or 1)
// 
#define	memory_ReadBit(src, off, bit_no)	\
	((*(((byte*)(src)) + (off) + ((bit_no) >> 3)) >> ((bit_no) & 7)) & 1)

#if memory_Shorthand >= 1
#define	memReadBit				memory_ReadBit
#endif

#if	memory_Shorthand >= 2
#define	ReadBit					memory_ReadBit
#endif



// Function:
// ReadNibble(*src, off, nibble_no)
// 
// Returns the value of nibble at the specified nibble number (nibble_no)
// from the specified source address with offset (src + off).
// 
// Parameters:
// *src:		the base address of stored data
// off:			offset from which nibble index starts
// nibble_no:	the index of the nibble (starts from 0)
// 
// Returns:
// value:		the value of the specified nibble (0 to 15 or 0x0 to 0xF)
// 
#define	memory_ReadNibble(src, off, nibble_no)	\
	((*(((byte*)(src)) + (off) + ((nibble_no) >> 1)) >> (((nibble_no) & 1) << 2)) & 0xF)

#if memory_Shorthand >= 1
#define	memReadNibble			memory_ReadNibble
#endif

#if	memory_Shorthand >= 2
#define	ReadNibble				memory_ReadNibble
#endif



// Function:
// Read<Type>(*src, off)
// 
// Returns the <type> value at the specified source address with 
// offset (src + off).
// 
// Parameters:
// *src:	the base address of stored data
// off:		offset of the <type> value
// 
// Returns:
// value:	the value of the specified <type>
// 
#define	memory_ReadType(type, src, off)	\
	(*((type*)(((byte*)(src)) + (off))))

#define memory_ReadChar(src, off)	\
	memory_ReadType(char, src, off)

#define memory_ReadByte(src, off)	\
	memory_ReadType(byte, src, off)

#define memory_ReadSbyte(src, off)	\
	memory_ReadType(sbyte, src, off)

#define memory_ReadBoolean(src, off)	\
	(memory_ReadByte(src, off) != 0)

#define	memory_ReadBool(src, off)	\
	memory_ReadBoolean(src, off)

#define	memory_ReadShort(src, off)	\
	memory_ReadType(short, src, off)

#define	memory_ReadUshort(src, off)	\
	memory_ReadType(ushort, src, off)

#define	memory_ReadInt(src, off)	\
	memory_ReadType(int, src, off)

#define	memory_ReadUint(src, off)	\
	memory_ReadType(uint, src, off)

#define	memory_ReadLong(src, off)	\
	memory_ReadType(long, src, off)

#define	memory_ReadUlong(src, off)	\
	memory_ReadType(ulong, src, off)

#define	memory_ReadInt8(src, off)	\
	memory_ReadType(int8, src, off)

#define	memory_ReadUint8(src, off)	\
	memory_ReadType(uint8, src, off)

#define	memory_ReadInt16(src, off)	\
	memory_ReadType(int16, src, off)

#define	memory_ReadUint16(src, off)	\
	memory_ReadType(uint16, src, off)

#define	memory_ReadInt32(src, off)	\
	memory_ReadType(int32, src, off)

#define	memory_ReadUint32(src, off)	\
	memory_ReadType(uint32, src, off)

#define	memory_ReadInt64(src, off)	\
	memory_ReadType(int64, src, off)

#define	memory_ReadUint64(src, off)	\
	memory_ReadType(uint64, src, off)

#define	memory_ReadFloat(src, off)	\
	memory_ReadType(float, src, off)

#define	memory_ReadDouble(src, off)	\
	memory_ReadType(double, src, off)

#define	memory_ReadString(src, off)	\
	memory_ReadType(string, src, off)

#if memory_Shorthand >= 1
#define	memReadChar			memory_ReadChar
#define	memReadByte			memory_ReadByte
#define	memReadSbyte		memory_ReadSbyte
#define	memReadBoolean		memory_ReadBoolean
#define	memReadBool			memory_ReadBool
#define	memReadShort		memory_ReadShort
#define	memReadUshort		memory_ReadUshort
#define	memReadInt			memory_ReadInt
#define	memReadUint			memory_ReadUint
#define	memReadLong			memory_ReadLong
#define	memReadUlong		memory_ReadUlong
#define	memReadInt8			memory_ReadInt8
#define	memReadUint8		memory_ReadUint8
#define	memReadInt16		memory_ReadInt16
#define	memReadUint16		memory_ReadUint16
#define	memReadInt32		memory_ReadInt32
#define	memReadUint32		memory_ReadUint32
#define	memReadInt64		memory_ReadInt64
#define	memReadUint64		memory_ReadUint64
#define	memReadFloat		memory_ReadFloat
#define	memReadDouble		memory_ReadDouble
#define	memReadString		memory_ReadString
#endif

#if	memory_Shorthand >= 2
#define	ReadChar		memory_ReadChar
#define	ReadByte		memory_ReadByte
#define	ReadSbyte		memory_ReadSbyte
#define	ReadBoolean		memory_ReadBoolean
#define	ReadBool		memory_ReadBool
#define	ReadShort		memory_ReadShort
#define	ReadUshort		memory_ReadUshort
#define	ReadInt			memory_ReadInt
#define	ReadUint		memory_ReadUint
#define	ReadLong		memory_ReadLong
#define	ReadUlong		memory_ReadUlong
#define	ReadInt8		memory_ReadInt8
#define	ReadUint8		memory_ReadUint8
#define	ReadInt16		memory_ReadInt16
#define	ReadUint16		memory_ReadUint16
#define	ReadInt32		memory_ReadInt32
#define	ReadUint32		memory_ReadUint32
#define	ReadInt64		memory_ReadInt64
#define	ReadUint64		memory_ReadUint64
#define	ReadFloat		memory_ReadFloat
#define	ReadDouble		memory_ReadDouble
#define	ReadString		memory_ReadString
#endif



// Function:
// WriteBit(*dst, off, bit_no, bit_value)
// 
// Stores the value of bit at the specified bit number (bit_no)
// to the specified destination address with offset (dst + off).
// 
// Parameters:
// *dst:		the base address of destination
// off:			offset from which bit index starts
// bit_no:		the index of the bit (starts from 0)
// bit_value:	the value of the specified bit (0 or 1)
// 
// Returns:
// nothing
// 
#define	memory_WriteBit(dst, off, bit_no, bit_value)	\
		(*(((byte*)(dst)) + (off) + ((bit_no) >> 3)) = *(((byte*)(dst)) + (off) + ((bit_no) >> 3)) & (~(1 << ((byte)(bit_no) & (byte)7))) | (1 << ((byte)(bit_no) & (byte)7)))

#if	memory_Shorthand >= 1
#define	memWriteBit			memory_WriteBit
#endif

#if	memory_Shorthand >= 2
#define	WriteBit			memory_WriteBit
#endif



// Function:
// WriteNibble(*dst, off, nibble_no, nibble_value)
// 
// Stores the value of nibble at the specified nibble number
// (nibble_no) to the specified destination address with
// offset (dst + off).
// 
// Parameters:
// *dst:			the base address of destination
// off:				offset from which nibble index starts
// nibble_no:		the index of the nibble (starts from 0)
// nibble_value:	the value of the specified nibble (0 to 15 or 0x0 to 0xF)
// 
// Returns:
// nothing
// 
#define	memory_WriteNibble(dst, off, nibble_no, nibble_value)	\
	(*(((byte*)(dst)) + (off) + ((nibble_no) >> 1)) = (*(((byte*)(dst)) + (off) + ((nibble_no) >> 1)) & (~(0xF << (((nibble_no) & 1) << 2)))) | ((nibble_value) << (((nibble_no) & 1) << 2)))

#if	memory_Shorthand >= 1
#define	memWriteNibble			memory_WriteNibble
#endif

#if	memory_Shorthand >= 2
#define	WriteNibble				memory_WriteNibble
#endif



// Function:
// Write<Type>(*dst, off, value)
// 
// Stores the value of <type> at the specified destination
// address with offset (dst + off).
// 
// Parameters:
// *dst:	the base address of destination
// off:		offset where <type> value is stored
// value:	the value of <type> to be stored
// 
// Returns:
// nothing
// 
#define	memory_WriteType(type, dst, off, value)	\
	(*((type*)(((byte*)(dst)) + (off))) = (value))

#define memory_WriteChar(dst, off, value)	\
	memory_WriteType(char, dst, off, value)

#define memory_WriteByte(dst, off, value)	\
	memory_WriteType(byte, dst, off, value)

#define memory_WriteSbyte(dst, off, value)	\
	memory_WriteType(sbyte, dst, off, value)

#define memory_WriteBoolean(dst, off, value)	\
	memory_WriteByte(dst, off, ((value) != 0))

#define	memory_WriteBool(dst, off, value)	\
	memory_WriteBoolean(dst, off, value)

#define	memory_WriteShort(dst, off, value)	\
	memory_WriteType(short, dst, off, value)

#define	memory_WriteUshort(dst, off, value)	\
	memory_WriteType(ushort, dst, off, value)

#define	memory_WriteInt(dst, off, value)	\
	memory_WriteType(int, dst, off, value)

#define	memory_WriteUint(dst, off, value)	\
	memory_WriteType(uint, dst, off, value)

#define	memory_WriteLong(dst, off, value)	\
	memory_WriteType(long, dst, off, value)

#define	memory_WriteUlong(dst, off, value)	\
	memory_WriteType(ulong, dst, off, value)

#define	memory_WriteInt8(dst, off, value)	\
	memory_WriteType(int8, dst, off, value)

#define	memory_WriteUint8(dst, off, value)	\
	memory_WriteType(uint8, dst, off, value)

#define	memory_WriteInt16(dst, off, value)	\
	memory_WriteType(int16, dst, off, value)

#define	memory_WriteUint16(dst, off, value)	\
	memory_WriteType(uint16, dst, off, value)

#define	memory_WriteInt32(dst, off, value)	\
	memory_WriteType(int32, dst, off, value)

#define	memory_WriteUint32(dst, off, value)	\
	memory_WriteType(uint32, dst, off, value)

#define	memory_WriteInt64(dst, off, value)	\
	memory_WriteType(int64, dst, off, value)

#define	memory_WriteUint64(dst, off, value)	\
	memory_WriteType(uint64, dst, off, value)

#define	memory_WriteFloat(dst, off, value)	\
	memory_WriteType(float, dst, off, value)

#define	memory_WriteDouble(dst, off, value)	\
	memory_WriteType(double, dst, off, value)

#define	memory_WriteString(dst, off, value)	\
	memory_WriteType(string, dst, off, value)

#if	memory_Shorthand >= 1
#define	memWriteChar		memory_WriteChar
#define	memWriteByte		memory_WriteByte
#define	memWriteSbyte		memory_WriteSbyte
#define	memWriteBoolean		memory_WriteBoolean
#define	memWriteBool		memory_WriteBool
#define	memWriteShort		memory_WriteShort
#define	memWriteUshort		memory_WriteUshort
#define	memWriteInt			memory_WriteInt
#define	memWriteUint		memory_WriteUint
#define	memWriteLong		memory_WriteLong
#define	memWriteUlong		memory_WriteUlong
#define	memWriteInt8		memory_WriteInt8
#define	memWriteUint8		memory_WriteUint8
#define	memWriteInt16		memory_WriteInt16
#define	memWriteUint16		memory_WriteUint16
#define	memWriteInt32		memory_WriteInt32
#define	memWriteUint32		memory_WriteUint32
#define	memWriteInt64		memory_WriteInt64
#define	memWriteUint64		memory_WriteUint64
#define	memWriteFloat		memory_WriteFloat
#define	memWriteDouble		memory_WriteDouble
#define	memWriteString		memory_WriteString
#endif

#if	memory_Shorthand >= 2
#define	WriteChar		memory_WriteChar
#define	WriteByte		memory_WriteByte
#define	WriteSbyte		memory_WriteSbyte
#define	WriteBoolean	memory_WriteBoolean
#define	WriteBool		memory_WriteBool
#define	WriteShort		memory_WriteShort
#define	WriteUshort		memory_WriteUshort
#define	WriteInt		memory_WriteInt
#define	WriteUint		memory_WriteUint
#define	WriteLong		memory_WriteLong
#define	WriteUlong		memory_WriteUlong
#define	WriteInt8		memory_WriteInt8
#define	WriteUint8		memory_WriteUint8
#define	WriteInt16		memory_WriteInt16
#define	WriteUint16		memory_WriteUint16
#define	WriteInt32		memory_WriteInt32
#define	WriteUint32		memory_WriteUint32
#define	WriteInt64		memory_WriteInt64
#define	WriteUint64		memory_WriteUint64
#define	WriteFloat		memory_WriteFloat
#define	WriteDouble		memory_WriteDouble
#define	WriteString		memory_WriteString
#endif



// Function:
// Swap(a, b, temp)
// Swap(a, b)
// 
// Swaps two variables, using a third variable if provided
// 
// Parameters:
// a:		first variable
// b:		second variable
// temp:	temporary variable used for swapping
// 
// Returns:
// nothing
// 
#define	memory_Swap3(a, b, temp)	\
	MacroBegin	\
		temp = a;	\
		a = b;		\
		b = temp;	\
	MacroEnd

#define	memory_Swap2(a, b)	\
	MacroBegin	\
		a ^= b;	\
		b ^= a;	\
		a ^= b;	\
	MacroEnd

#define	memory_Swap(...)	\
	Macro(Macro3(__VA_ARGS__, memory_Swap3, memory_Swap2)(__VA_ARGS__))

#if	memory_Shorthand >= 1
#define	memSwap		memory_Swap
#endif

#if	memory_Shorthand >= 2
#define	Swap		memory_Swap
#endif



// Function:
// Init(*dst, off, size, value)
// Init(*dst, off, size)
// 
// Initializes a region of memory with a particular byte value, or "0" if unspecified.
// 
// Parameters:
// *dst:	destination memory base address
// off:		destination memory offset
// size:	size of memory region to initialize
// value:	value to be used for initialization (default 0)
// 
// Returns:
// nothing
// 
#if lib_Platform == lib_PlatformPC
#define	memory_InitV(dst, off, size, value)	\
	FillMemory((byte*)(dst) + (off), size, value)

#else
void memory_InitVF(byte* dst, uint size, byte value)
{
	byte i;
	while(size > 0)
	{
		i = size & 0xFF;
		do
		{
			*dst = value;
			dst++;
			i--;
		}while(i != 0);
		size -= i;
	}
}

#define	memory_InitV(dst, off, size, value)	\
	memory_InitVF((byte*)(dst) + (off), (uint)(size), (byte)(value))
#endif

#if lib_Platform == lib_PlatformPC
#define	memory_InitZ(dst, off, size)	\
	ZeroMemory((byte*)(dst) + (off), size)

#else
#define	memory_InitZ(dst, off, size)	\
	memory_InitV(dst, off, size, 0)
#endif

#define	memory_Init(...)	\
	Macro(Macro4(__VA_ARGS__, memory_InitV, memory_InitZ)(__VA_ARGS__))

#if	memory_Shorthand >= 1
#define	memInit		memory_Init
#endif

#if	memory_Shorthand >= 2
#define	Init		memory_Init
#endif



// Function:
// NewRegion(maxsz)
// NewRegion(initsz, maxsz)
// 
// Gets a region of memory for memory allocation (heap)
// 
// Parameters:
// initsz:	initial size of memory region
// maxsz:	maximum size of memory region
// 
// Returns:
// mem_rgn:	newly created memory region handle
// 
#if lib_Platform == lib_PlatformPC
#define	memory_NewRegionM(maxsz)	\
	HeapCreate((DWORD)0, (SIZE_T)0, (SIZE_T)(maxsz))

#define	memory_NewRegionIM(initsz, maxsz)	\
	HeapCreate((DWORD)0, (SIZE_T)(initsz), (SIZE_T)(maxsz))

#define	memory_NewRegion(...)	\
	Macro(Macro2(__VA_ARGS__, memory_NewRegionIM, memory_NewRegionM)(__VA_ARGS__))

#else
#define	memory_NewRegion(...)

#endif

#define	memory_Region		HANDLE

#if	memory_Shorthand >= 1
#define	memNewRegion		memory_NewRegion
#define	memRegion			memory_Region
#endif

#if	memory_Shorthand >= 2
#define	NewRegion			memory_NewRegion
#define	Region				memory_Region
#endif



// Function:
// DeleteRegion(mem_rgn)
// 
// Deletes a memory region
// 
// Parameters:
// mem_rgn:	memory region to be deleted
// 
// Returns:
// nothing
// 
#if lib_Platform == lib_PlatformPC
#define	memory_DeleteRegion(mem_rgn)	\
	HeapDestroy((HANDLE)mem_rgn)

#else
#define	memory_DeleteRegion(...)

#endif

#if	memory_Shorthand >= 1
#define	memDeleteRegion		memory_DeleteRegion
#endif

#if	memory_Shorthand >= 2
#define	DeleteRegion		memory_DeleteRegion
#endif



// Function:
// Alloc(mem_rgn, size)
// Alloc(mem_rgn, size, options)
// 
// Allocates memory from a memory region and return pointer
// to the allocated memory.
// 
// Parameters:
// mem_rgn:	memory region to be used for allocation
// size:	size of memory to be allocated
// options:	options (INIT_TO_ZERO)
// 
// Returns:
// *ptr:	pointer to allocated memory
// 
#if lib_Platform == lib_PlatformPC
#define	memory_AllocS(mem_rgn, size)	\
	HeapAlloc((HANDLE)(mem_rgn), (DWORD)0, (SIZE_T)(size))

#define	memory_AllocSO(mem_rgn, size, options)	\
	HeapAlloc((HANDLE)(mem_rgn), (DWORD)(options), (SIZE_T)(size))

#define	memory_Alloc(...)	\
	Macro(Macro3(__VA_ARGS__, memory_AllocSO, memory_AllocS)(__VA_ARGS__))

#else
#define	memory_Alloc(...)

#endif

#define	memory_INIT_TO_ZERO	\
	HEAP_ZERO_MEMORY

#if	memory_Shorthand >= 1
#define	memAlloc			memory_Alloc
#define	memINIT_TO_ZERO		memory_INIT_TO_ZERO
#endif

#if	memory_Shorthand >= 2
#define	Alloc				memory_Alloc
#define	INIT_TO_ZERO		memory_INIT_TO_ZERO
#endif



// Function:
// ReAlloc(mem_rgn, ptr, size)
// ReAlloc(mem_rgn, ptr, size, options)
// 
// Reallocates an allocated memory to a new size from a memory region and return pointer
// to the reallocated memory.
// 
// Parameters:
// mem_rgn:	memory region to be used for reallocation
// ptr:		allocated memory that is to be reallocated
// size:	size of memory to be allocated
// options:	options (INITIALIZE_TO_ZERO)
// 
// Returns:
// *ptr:	pointer to reallocated memory
// 
#if lib_Platform == lib_PlatformPC
#define	memory_ReAllocS(mem_rgn, ptr, size)	\
	HeapReAlloc((HANDLE)(mem_rgn), (DWORD)0, (LPVOID)(ptr), (SIZE_T)(size))

#define	memory_ReAllocSO(mem_rgn, ptr, size, options)	\
	HeapReAlloc((HANDLE)(mem_rgn), (DWORD)(options), (LPVOID)(ptr), (SIZE_T)(size))

#define	memory_ReAlloc(...)	\
	Macro(Macro4(__VA_ARGS__, memory_ReAllocSO, memory_ReAllocS)(__VA_ARGS__))

#else
#define	memory_ReAlloc(...)

#endif

#if	memory_Shorthand >= 1
#define	memReAlloc		memory_ReAlloc
#endif

#if	memory_Shorthand >= 2
#define	ReAlloc			memory_ReAlloc
#endif



// Function:
// Free(mem_rgn, ptr)
// 
// Frees an allocated memory from a memory region.
// 
// Parameters:
// mem_rgn:	memory region of the allocated memory
// ptr:		allocated memory that is to be freed
// 
// Returns:
// nothing
// 
#if lib_Platform == lib_PlatformPC
#define	memory_Free(mem_rgn, ptr)	\
	HeapFree((HANDLE)(mem_rgn), (DWORD)0, (LPVOID)(ptr))

#else
#define	memory_Free(...)

#endif

#if	memory_Shorthand >= 1
#define	memFree		memory_Free
#endif

#if	memory_Shorthand >= 2
#define	Free		memory_Free
#endif



// Function:
// Copy(*dst, doff, *src, soff, size)
// 
// Copies a region of source memory to a destination address.
// 
// Parameters:
// *dst:	destination memory base address
// doff:	destination memory offset
// *src:	source memory base address
// soff:	source memory offset
// size:	size of source memory region to copy
// 
// Returns:
// nothing
// 
#if lib_Platform == lib_PlatformPC
#define	memory_Copy(dst, doff, src, soff, size)	\
	CopyMemory((byte*)(dst) + (doff), (byte*)(src) + (soff), size)

#else
void memory_CopyF(byte* dst, byte* src, uint size)
{
	byte i;
	if(dst < src)
	{
		while(size > 0)
		{
			i = size & 0xFF;
			do
			{
				*dst = *src;
				dst++;
				src++;
				i--;
			}while(i != 0);
			size -= i;
		}
	}
	else
	{
		dst += size - 1;
		src += size - 1;
		while(size > 0)
		{
			i = size & 0xFF;
			do
			{
				*dst = *src;
				dst--;
				src--;
				i--;
			}while(i != 0);
			size -= i;
		}
	}
}

#define	memory_Copy(dst, doff, src, soff, size)	\
	memory_CopyF(((byte*)(dst) + (doff)), ((byte*)(src) + (soff)), (uint)(size))
#endif

#if	memory_Shorthand >= 1
#define	memCopy			memory_Copy
#endif

#if	memory_Shorthand >= 2
#define	Copy			memory_Copy
#endif



// Function:
// Reverse(*src, off, size)
// Reverse<Type>(*src, off)
// 
// Reverses the data stored at the source address (src + off)
// of specified length (len). The data at the source is directly
// reversed, and hence if the original data is required, then
// it is suggested to make a copy of it.
// 
// Parameters:
// src:		the base address of source data
// off:		offset to the actual data to be reversed (src + off)
// size:	size of data to be reversed
// 
// Returns:
// nothing
// 
void memory_ReverseF(byte* src, uint size)
{
	byte byt, *end;
	for(end=src+size-1; src<end; src++, end--)
	{
		byt = *src;
		*src = *end;
		*end = byt;
	}
}

#define	memory_Reverse(src, off, size)	\
	memory_ReverseF((byte*)(src) + (off), (uint)(size))

#define	memory_Reverse2(src, off)	\
	memory_Swap(*((byte*)(src) + (off)), *((byte*)(src) + (off) + 1))

#define	memory_Reverse4(src, off)	\
	MacroBegin	\
		byte temp;	\
		temp = *((byte*)(src) + (off));	\
		*((byte*)(src) + (off)) = *((byte*)(src) + (off) + 3);	\
		*((byte*)(src) + (off) + 3) = temp;	\
		temp = *((byte*)(src) + (off) + 1);	\
		*((byte*)(src) + (off) + 1) = *((byte*)(src) + (off) + 2);	\
		*((byte*)(src) + (off) + 2) = temp;	\
	MacroEnd

#define	memory_Reverse8(src, off)	\
	memory_Reverse(src, off, 8)

#define memory_ReverseChar(src, off)

#define memory_ReverseByte(src, off)

#define memory_ReverseSbyte(src, off)

#define memory_ReverseBoolean(src, off)

#define	memory_ReverseBool(src, off)

#define	memory_ReverseShort(src, off)	\
	memory_Reverse2(src, off)

#define	memory_ReverseUshort(src, off)	\
	memory_Reverse2(src, off)

#if WaspSrvr_WordSize <= 16
#define	memory_ReverseInt(src, off)	\
	memory_Reverse2(src, off)

#define	memory_ReverseUint(src, off)	\
	memory_Reverse2(src, off)
#else
#define	memory_ReverseInt(src, off)	\
	memory_Reverse4(src, off)

#define	memory_ReverseUint(src, off)	\
	memory_Reverse4(src, off)
#endif

#define	memory_ReverseLong(src, off)	\
	memory_Reverse4(src, off)

#define	memory_ReverseUlong(src, off)	\
	memory_Reverse4(src, off)

#define	memory_ReverseInt8(src, off)

#define	memory_ReverseUint8(src, off)

#define	memory_ReverseInt16(src, off)	\
	memory_Reverse2(src, off)

#define	memory_ReverseUint16(src, off)	\
	memory_Reverse2(src, off)

#define	memory_ReverseInt32(src, off)	\
	memory_Reverse4(src, off)

#define	memory_ReverseUint32(src, off)	\
	memory_Reverse4(src, off)

#define	memory_ReverseInt64(src, off)	\
	memory_Reverse8(src, off)

#define	memory_ReverseUint64(src, off)	\
	memory_Reverse8(src, off)

#define	memory_ReverseFloat(src, off)	\
	memory_Reverse4(src, off)

#define	memory_ReverseDouble(src, off)	\
	memory_Reverse8(src, off)

#if WaspSrvr_WordSize <= 16
#define	memory_ReverseString(src, off)	\
	memory_Reverse2(src, off)
#else
#define	memory_ReverseString(src, off)	\
	memory_Reverse4(src, off)
#endif

#if	memory_Shorthand >= 1
#define	memReverse				memory_Reverse
#define	memReverseChar			memory_ReverseChar
#define	memReverseByte			memory_ReverseByte
#define	memReverseSbyte			memory_ReverseSbyte
#define	memReverseBoolean		memory_ReverseBoolean
#define	memReverseBool			memory_ReverseBool
#define	memReverseShort			memory_ReverseShort
#define	memReverseUshort		memory_ReverseUshort
#define	memReverseInt			memory_ReverseInt
#define	memReverseUint			memory_ReverseUint
#define	memReverseLong			memory_ReverseLong
#define	memReverseUlong			memory_ReverseUlong
#define	memReverseInt8			memory_ReverseInt8
#define	memReverseUint8			memory_ReverseUint8
#define	memReverseInt16			memory_ReverseInt16
#define	memReverseUint16		memory_ReverseUint16
#define	memReverseInt32			memory_ReverseInt32
#define	memReverseUint32		memory_ReverseUint32
#define	memReverseInt64			memory_ReverseInt64
#define	memReverseUint64		memory_ReverseUint64
#define	memReverseFloat			memory_ReverseFloat
#define	memReverseDouble		memory_ReverseDouble
#define	memReverseString		memory_ReverseString
#endif

#if	memory_Shorthand >= 2
#define	Reverse				memory_Reverse
#define	ReverseChar			memory_ReverseChar
#define	ReverseByte			memory_ReverseByte
#define	ReverseSbyte		memory_ReverseSbyte
#define	ReverseBoolean		memory_ReverseBoolean
#define	ReverseBool			memory_ReverseBool
#define	ReverseShort		memory_ReverseShort
#define	ReverseUshort		memory_ReverseUshort
#define	ReverseInt			memory_ReverseInt
#define	ReverseUint			memory_ReverseUint
#define	ReverseLong			memory_ReverseLong
#define	ReverseUlong		memory_ReverseUlong
#define	ReverseInt8			memory_ReverseInt8
#define	ReverseUint8		memory_ReverseUint8
#define	ReverseInt16		memory_ReverseInt16
#define	ReverseUint16		memory_ReverseUint16
#define	ReverseInt32		memory_ReverseInt32
#define	ReverseUint32		memory_ReverseUint32
#define	ReverseInt64		memory_ReverseInt64
#define	ReverseUint64		memory_ReverseUint64
#define	ReverseFloat		memory_ReverseFloat
#define	ReverseDouble		memory_ReverseDouble
#define	ReverseString		memory_ReverseString
#endif



#endif
