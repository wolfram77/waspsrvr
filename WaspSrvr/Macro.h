/*
----------------------------------------------------------------------------------------
	Macro: Macro overloading support module
	File: Macro.h

	This file is part of WaspSrvr. For more details, go through
	Readme.txt. For copyright information, go through copyright.txt.

	WaspSrvr is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	WaspSrvr is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with WaspSrvr.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------------------
*/



/*
	Macro is a macro overloading support module for WaspSrvr. It has been developed for
	for being used in the development of the internet of things. For information,
	on its usage, please visit: https://github.com/wolfram77/WaspSrvr.
*/



#ifndef	_Macro_h_
#define	_Macro_h_



// Shorthand level
// 
// The default shorthand level is 2 i.e., members of this
// module can be accessed as <function_name> directly.
// This shorthand level cannot be changed
#ifndef	macro_Shorthand
#define	macro_Shorthand		2
#endif



// Support macro overloading
// By default, C/C++ does not support macro overloading, but
// through the use of variable argument macros called variadic
// macros, it is possible to support macro overloading to some
// extent
#ifndef	Macro
#define	Macro(x)	x
#define	Macro1(_1, func, ...)	func
#define	Macro2(_1, _2, func, ...)	func
#define	Macro3(_1, _2, _3, func, ...)	func
#define	Macro4(_1, _2, _3, _4, func, ...)	func
#define	Macro5(_1, _2, _3, _4, _5, func, ...)	func
#define	Macro6(_1, _2, _3, _4, _5, _6, func, ...)	func
#define	Macro7(_1, _2, _3, _4, _5, _6, _7, func, ...)	func
#define	Macro8(_1, _2, _3, _4, _5, _6, _7, _8, func, ...)	func
#define	Macro9(_1, _2, _3, _4, _5, _6, _7, _8, _9, func, ...)	func
#define	Macro10(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, func, ...)	func
#define	Macro11(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, func, ...)	func
#define	Macro12(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, func, ...)	func
#define	Macro13(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, func, ...)	func
#define	Macro14(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, func, ...)	func
#define	Macro15(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, func, ...)	func
#define	Macro16(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, func, ...)	func
#define	Macro17(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, func, ...)	func
#define	Macro18(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, func, ...)	func
#define	Macro19(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, func, ...)	func
#define	Macro20(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, func, ...)	func
#define	Macro21(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, func, ...)	func
#define	Macro22(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, func, ...)	func
#define	Macro23(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, func, ...)	func
#define	Macro24(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, func, ...)	func
#define	EmptyMacro(...)
#if lib_Platform == lib_PlatformPC
#define	MacroBegin	\
	do{
#define	MacroEnd	\
	}while(0)
#else
#define	MacroBegin	\
	({
#define	MacroEnd	\
	})
#endif
#endif


#endif
